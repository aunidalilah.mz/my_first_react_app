/*
 * --------------------------------------------------------------------
 * Project: My First Create React App
 * Version: 0.1.1
 * File: MovieCard.jsx
 * Created: Friday, 1st July 2022 4:25:19 pm
 * Modified: Friday, 1st July 2022 4:25:19 pm
 * Author: Auni Dalilah Mohd Zain (auni@scs.my)
 *
 * Copyright (C) 2022 - System Consultancy Services Sdn. Bhd.
 * --------------------------------------------------------------------
 */

import React from "react";

const MovieCard = ({ movie: { imdbID, Year, Poster, Title, Type } }) => {
  return (
    <div className="movie" key={imdbID}>
      <div>
        <p>{Year}</p>
      </div>

      <div>
        <img
          src={Poster !== "N/A" ? Poster : "https://via.placeholder.com/400"}
          alt={Title}
        />
      </div>

      <div>
        <span>{Type}</span>
        <h3>{Title}</h3>
      </div>
    </div>
  );
};

export default MovieCard;
