/*
 * --------------------------------------------------------------------
 * Project: My First Create React App
 * Version: 0.1.1
 * File: index.js
 * Created: Friday, 1st July 2022 10:58:18 am
 * Modified: Friday, 1st July 2022 12:10:39 pm
 * Author: Auni Dalilah Mohd Zain (auni@scs.my)
 *
 * Copyright (C) 2022 - System Consultancy Services Sdn. Bhd.
 * --------------------------------------------------------------------
 */

import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

ReactDOM.render(<App />, document.getElementById("root"));
